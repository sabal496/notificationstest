package com.example.notifications

import android.app.Notification
import android.app.NotificationChannel

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.util.Log.d
import androidx.core.app.NotificationCompat

import com.onesignal.NotificationExtenderService
import com.onesignal.OSNotificationReceivedResult

class NotificationService:NotificationExtenderService() {
    override fun onNotificationProcessing(notification: OSNotificationReceivedResult?): Boolean {
        d("jimsheri","${notification?.payload?.additionalData}")
        val jsonobject=notification?.payload?.additionalData;
        val id= jsonobject?.getString("id")
        val name= jsonobject?.getString("name")
        val surname= jsonobject?.getString("surname")
        val notificationManager=getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val appname="exampleNotification"
        val desctext="some desc"
        val importance=NotificationManager.IMPORTANCE_DEFAULT
        val channel= NotificationChannel(id,appname,importance).apply {
            description=desctext
        }
        notificationManager.createNotificationChannel(channel)

        val intent=Intent(applicationContext,MainActivity::class.java)
        val pendingIntent=PendingIntent.getActivity(applicationContext,1,intent,PendingIntent.FLAG_UPDATE_CURRENT)

        var builder = NotificationCompat.Builder(this, id!!)
        .setSmallIcon(R.mipmap.ic_launcher)
            .setContentTitle(name)
            .setContentText(surname)
            .setContentIntent(pendingIntent)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            notificationManager.notify(1,builder.build())

        return  false
     }
}