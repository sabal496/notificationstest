package com.example.notifications

import android.app.Application
import android.content.Context
import com.onesignal.OneSignal

class App:Application() {
        companion object{
            lateinit var instance:App
            var context:Context?=null

        }

    override fun onCreate() {
        super.onCreate()
        instance=this
        context=applicationContext
        initionesignal()
    }
    private fun initionesignal(){
        OneSignal.startInit(this)
            .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
            .unsubscribeWhenNotificationsAreDisabled(true)
            .init()
    }
}